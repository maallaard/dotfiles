local options = {
    termguicolors = true,
    fileencoding = "utf-8",
    clipboard = "unnamedplus",
    splitbelow = true,
    splitright = true,
    mouse = "a",

    backup = false,
    writebackup = false,
    swapfile = false,
    undofile = true,

    tabstop = 2,
    shiftwidth = 2,
    smarttab = true,
    smartindent = true,
    expandtab = true,

    number = true,
    numberwidth = 4,
    cursorline = true,

    wrap = true,
    linebreak = true,
    showmode = true,
    cmdheight = 2,

    showmatch = true,
    ignorecase = true,
    smartcase = true,
    conceallevel = 0,

    list = true,
    listchars = "tab:-->,trail:.,nbsp:+",
    shortmess = "atToOFS",
}

for k, v in pairs(options) do
    vim.opt[k] = v
end
